#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ESP8266TrueRandom.h>
#include <Wire.h>
#include <Adafruit_MCP23017.h>
#include <TM1637Display.h>
//#include <Adafruit_ADS1015.h>
#include "MQ135.h"
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <EEPROM.h>

/*------------------------- subscribe ----------------------------------------------*/
//const char* heatfan_power_set_suffix = "/power_set";
const char* heat_power_set_suffix = "/heat/power_set";       // power on setting of heat
const char* fan_power_set_suffix = "/fan/power_set";         // power on setting of fan
const char* temp_set_suffix = "/temp_set";                   // set temperature value

/*------------------------publish----------------------------------------------*/

const char* operation_mode_state_suffix = "/operation_mode/state";  // state of operation mode
//const char* heatfan_power_state_suffix = "/power_state";
const char* heat_power_state_suffix = "/heat/power_state";          // power state of heat
const char* fan_power_state_suffix = "/fan/power_state";           // power state of fan
const char* curr_temp_suffix = "/curr_temp";                        // current measured temperature
const char* curr_humidity_suffix = "/curr_humidity";                // current measured humidity
const char* temp_alarm_suffix = "/alarm_state";                    // alarm state of heatfan
const char* curr_dustDensity_suffix = "/curr_dustDensity";      // density of dust in air
const char* curr_CO2_density_suffix = "/curr_CO_density";      // density of CO2 in air

const char* set_temp_suffix = "/set_temp_value";

/*-----------------------define pins----------------------------------------------*/

// define MCP21307 pins
/* Connect pin #12 of the expander to GPIO5 of ESP8266 (I2C SCL)
   Connect pin #13 of the expander to GPIO4 of ESP8266 (I2C SDA)
   Connect pins #15, 16 and 17 of the expander to ground (address selection)
   Connect pin #9 of the expander to 5V (power)
   Connect pin #10 of the expander to ground (common ground)
   Connect pin #18 through a ~10kohm resistor to 5V (reset pin, active low) */

// output pins of MCP21307
//byte heatfan_powerOn = 12;
byte heat_powerOn = 1;    // setup pin of heat power - GPA1 pin of MCP21307
byte fan_powerOn = 0;     // setup pin of fan power - GPA0 pin of MCP21307
//byte heatfan_powerLed = 10;
byte heat_powerLed = 10;   // setup pin indicated state of heat power - GPB2 pin of MCP21307
byte fan_powerLed = 11;    // setup pin indicated state of fan power - GPB3 pin of MCP21307
byte auto_modeLed = 8;     // setup pin indicated auto mode of heatfan - GPB0 pin of MCP21307
byte manu_modeLed = 9;     // setup pin indicated manual mode of heatfan - GPB1 pin of MCP21307
byte alarmLed = 12;     // setup pin indecated overalarm state of heatfan - GPB4 pin of MCP21307
byte manu_fanLed = 14;     // set led pin for indicating of manual fan operarion(GPB6)
byte manu_heatLed = 13;    // set led pin for indicating of manual heat(GPB5)

// input pins of MCP21307
byte operation_modeButt = 7;  // setup operation mode of heatfan - GPA7 pin of MCP21307
byte heat_onButt = 6;         // start heat in manual mode - GPA6 pin of MCP21307
byte fan_onButt = 5;          // start fan in manual mode - GPA5 pin of MCP21307
byte temp_setButt = 4;        // setup temperature in manual mode - GPA4 pin of MCP21307
byte temp_incButt = 3;        // increase by 1 degree of temperature in manual mode - GPA3 pin of MCP21307
byte temp_decButt = 2;        // decrease by 1 degree of temperature in manual mode - GPA2 pin of MCP21307
//byte heatfan_onButt = 3;

// define NodeMUC pins
// GPIO5 and GPIO4 connect to SCL and SDA pin of MCP21307

// define clk and dio of TM1637
#define T1_CLK 12   // use GPIO 12, define clk of TM1637 (current temperature display)
#define T1_DIO 14   // use GPIO 14, define dio of TM1637 (current temperature display)
#define T2_CLK 0    // use GPIO 0, define clk of TM1637 (setting temperature display)
#define T2_DIO 2    // use GPIO 2, define dio of TM1637 (setting temperature display)

// SDA and SCL pins of th ADC1015 module is wired to GPIO4 and GPIO5 of NODEMCU for I2C
byte pulseLedPin = 15;  //GPB7 pin of MCP23017-This pin is pin to drive led for GP2Y1010AU0F dust sensor module
int dustPin = A0; // analog output of dust sensor   #define AnalogPin A0

// define pin for MQ135 co2 sensor
// MQ135 co2 sensor module is wired to analog 0 pin on ADC1015 module
// ADC1015 module communicates with NODEMCU via I2C(GPIO5 and GPIO4)

/*    --------  Constant values -----------        */

const char* ssid = "WiFi560-AP";              // RPi3's AP name
const char* password = "Shane_wifi560";       // RPi3's AP password
const char* mqtt_server = "172.24.1.1";       // Server has been built on the router(RPi 3) itself

const char* device_name = "heatfan";

#define SEALEVELPRESSURE_HPA (1013.25) // This is for read altitude value of BME280 sensor

#define TEST_DELAY 300  // This is interval between on and off for display

/*--------------- class member-------------------*/

WiFiClient espClient;
PubSubClient client(espClient);

Adafruit_MCP23017 mcp;

TM1637Display display1(T1_CLK, T1_DIO);
TM1637Display display2(T2_CLK, T2_DIO);

Adafruit_BME280 bme; // I2C (BME280 sensor communicates with esp8266 via I2C)

Adafruit_ADS1015 ads;     /* Use thi for the 12-bit version */
MQ135 gasSensor = MQ135(0);  // AOUT of MQ135 gas sensor is connected to AO pin of ADS1015 module

/*--------------------------- Global variables-------------------------*/

float perOffset = 3; // permissible offset of temperature

char* val;           // val is payload value for topic of user setting temperature value
float temp_set_value;  // initiate temp value for setting of temperature on a auto mode
long m_temp = 15;      // initiate temp value for setting of temperature on a manual mode

float bme280_temp, bme280_humidity;

float adc_vol, dustDensity;  // 0th output voltage of ADC1015 module, density of dust

float rzero;   //this to get the rzero value, uncomment this to get ppm value
float co2Density;  //this to get ppm value, uncomment this to get rzero value

// Interrupts from the MCP will be handled by this PIN.
// This pin connect to INT A(or INT B)pin of MCP23017 in circuit.
byte esp8266IntPin = 13;  // use GPIO13 of NODEMCU
int butt_memo;  //variable for memoring of temp_setButt state on manual mode

byte uuidNumber[16]; // UUIDs in binary form are 16 bytes long
String uuid_buf;     // UUID in string type
char uuid_name[37];  // UUID in char array type

char* buf_bme280_temp = new char[10];
char* buf_bme280_humidity = new char[10];
char* buf_co2Density = new char[10];
char* buf_set_temp_value = new char[10];
char* buf_dustDensity = new char[10];


char buf_pub_topic[50];
char buf_sub_topic[50];

const char* heatfan_power_state = "OFF";
const char* heat_power_state = "OFF";
const char* fan_power_state = "OFF";
const char* operation_mode = "AUTO";
const char* temp_alarm = "OFF";

void setup() {

  Serial.begin(115200);

  EEPROM.begin(512);  // store values of relay state and brightness level setting

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  delay(1000);

  // setup pins for TM1637
  //pinMode(T1_CLK, OUTPUT);
  //pinMode(T1_DIO, OUTPUT);
  //pinMode(T2_CLK, OUTPUT);
  //pinMode(T2_DIO, OUTPUT);
  display1.setBrightness(7, true);
  display2.setBrightness(7, true);

  pinMode(esp8266IntPin, INPUT);
  mcp.begin();   // use default address 0

  // We mirror INTA and INTB, so that only one line is required between MCP and Arduino for int reporting
  // The INTA/B will not be Floating
  // INTs will be signaled with a LOW
  mcp.setupInterrupts(true, false, LOW);

  // setup pins for MCP23017
  mcp.pinMode(operation_modeButt, INPUT);
  mcp.pullUp(operation_modeButt, HIGH);  // turn on a 100K pullup internally

  mcp.pinMode(temp_setButt, INPUT);      // button for interrupt
  mcp.pullUp(temp_setButt, HIGH);
  mcp.setupInterruptPin(temp_setButt, CHANGE);
  mcp.pinMode(temp_incButt, INPUT);
  mcp.pullUp(temp_incButt, HIGH);
  mcp.setupInterruptPin(temp_incButt, CHANGE);
  mcp.pinMode(temp_decButt, INPUT);
  mcp.pullUp(temp_decButt, HIGH);
  mcp.setupInterruptPin(temp_decButt, CHANGE);

  //mcp.pinMode(heatfan_onButt, INPUT);
  //mcp.pullUp(heatfan_onButt, HIGH);
  mcp.pinMode(heat_onButt, INPUT);
  mcp.pullUp(heat_onButt, HIGH);
  mcp.pinMode(fan_onButt, INPUT);
  mcp.pullUp(fan_onButt, HIGH);

  //mcp.pinMode(heatfan_powerOn, OUTPUT);
  mcp.pinMode(heat_powerOn, OUTPUT);
  mcp.pinMode(fan_powerOn, OUTPUT);
  //mcp.pinMode(heatfan_powerLed, OUTPUT);
  mcp.pinMode(heat_powerLed, OUTPUT);
  mcp.pinMode(fan_powerLed, OUTPUT);
  mcp.pinMode(auto_modeLed, OUTPUT);
  mcp.pinMode(manu_modeLed, OUTPUT);
  mcp.pinMode(alarmLed, OUTPUT);
  mcp.pinMode(manu_heatLed, OUTPUT);
  mcp.pinMode(manu_fanLed, OUTPUT);

  mcp.pinMode(pulseLedPin, OUTPUT);   //This pin is to drive led for dust sensor module

  attachInterrupt(esp8266IntPin, handle_ButtInterrupt, FALLING); /*interrupt for setting of temperature in manual mode*/
  //This will clear interrupts on MCP prior to entering main loop
  mcp.readGPIOAB();

  // Initialize BME280 sensor. The I2C device address of BME280 sensor is 0x76.
  Serial.println("BME280 is beginning.");
  if (!bme.begin(0x76)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }

  ///////////ads.begin();    // for ADC1015

  ESP8266TrueRandom.uuid(uuidNumber);  // uuid of device
  Serial.print("The UUID number is ");
  printUuid(uuidNumber);
  Serial.println();
  uuid_buf = ESP8266TrueRandom.uuidToString(uuidNumber);
  uuid_buf.toCharArray(uuid_name, 37);
  Serial.println(uuid_name);
}

void setup_wifi() {

  int attempt = 0;

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {

    // Try to connect for 15 sec, and restart nodemcu
    if (attempt < 30)
      attempt ++;
    else
      ESP.restart();
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println("");
  //***********************************************************************************************
  // Check topic of heat power setting, and than control powerOn led, power on the heat.
  /*set_sub_topic(heatfan_power_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

    if (!strncmp((const char*)payload, "ON", 2)) {

      Serial.println("The heatfan is turning to ON...");
      digitalWrite(heatfan_powerLed, HIGH);
      digitalWrite(heatfan_powerOn, HIGH);
      heatfan_power_state = "ON";
    }
    else if (!strncmp((const char*)payload, "OFF", 3)) {

      Serial.println("The heatfan is turning to OFF ...");
      digitalWrite(heatfan_powerLed, LOW);
      digitalWrite(heatfan_powerOn, LOW);
      heatfan_power_state = "OFF";
    }
  }
  // Check topic of temperature setting, and than resetup temperature value.
  else {
    set_sub_topic(temp_set_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

      char* val = (char*)payload;
      temp_set_value = atoi(val);
      Serial.print("User setting value of temperature is ");
      Serial.println(temp_set_value);

      Display2_set_temp(temp_set_value);
    }
  }
  EEPROM_Write();
  }*/
  /*******************************************************************************/
  // Check topic of heat power setting, and than control powerOn led, power on the heat.
  set_sub_topic(heat_power_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

    if (!strncmp((const char*)payload, "ON", 2)) {

      Serial.println("The heat is turning to ON...");
      digitalWrite(heat_powerLed, HIGH);
      digitalWrite(heat_powerOn, HIGH);
      heat_power_state = "ON";
    }
    else if (!strncmp((const char*)payload, "OFF", 3)) {

      Serial.println("The heat is turning to OFF ...");
      digitalWrite(heat_powerLed, LOW);
      digitalWrite(heat_powerOn, LOW);
      heat_power_state = "OFF";
    }
  }
  // Check topic of fan power setting, and than control powerOn led, power on the fan.
  else {
    set_sub_topic(fan_power_set_suffix);
    if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

      if (!strncmp((const char*)payload, "ON", 2)) {

        Serial.println("The fan is turning to ON...");
        digitalWrite(fan_powerLed, HIGH);
        digitalWrite(fan_powerOn, HIGH);
        fan_power_state = "ON";
      }
      else if (!strncmp((const char*)payload, "OFF", 3)) {

        Serial.println("The fan is turning to OFF ...");
        digitalWrite(fan_powerLed, LOW);
        digitalWrite(fan_powerOn, LOW);
        fan_power_state = "OFF";
      }
    }
  }
  // Check topic of temperature setting, and than resetup temperature value.
  set_sub_topic(temp_set_suffix);
  if (strncmp(buf_sub_topic, topic, strlen(topic)) == 0) {

    val = (char*)payload;
    temp_set_value = atoi(val);
    Serial.print("User setting value of temperature is ");
    Serial.println(temp_set_value);

    Display2_set_temp(temp_set_value);

    EEPROM_Write();
  }
  //EEPROM_Write();
}

void printHex(byte number) {
  int topDigit = number >> 4;
  int bottomDigit = number & 0x0f;
  // Print high hex digit
  Serial.print( "0123456789ABCDEF"[topDigit] );
  // Low hex digit
  Serial.print( "0123456789ABCDEF"[bottomDigit] );
}

void printUuid(byte* uuidNumber) {
  int i;
  for (i = 0; i < 16; i++) {
    if (i == 4) Serial.print("-");
    if (i == 6) Serial.print("-");
    if (i == 8) Serial.print("-");
    if (i == 10) Serial.print("-");
    printHex(uuidNumber[i]);
  }
}

void reconnect() {

  int attempt = 0;

  // Loop until we're reconnected
  while (!client.connected()) {

    if (attempt < 3)
      attempt ++;
    else
      ESP.restart();

    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(uuid_name)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("overall_topic", "hello world");
      // ... and resubscribe

      //set_sub_topic(heatfan_power_set_suffix);
      //client.subscribe(buf_sub_topic);

      set_sub_topic(heat_power_set_suffix);
      client.subscribe(buf_sub_topic);

      set_sub_topic(fan_power_set_suffix);
      client.subscribe(buf_sub_topic);

      set_sub_topic(temp_set_suffix);
      client.subscribe(buf_sub_topic);

      //client.subscribe("common");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 3 seconds before retrying
      delay(3000);
    }
  }
}

void handle_ButtInterrupt() {

  Serial.println("Interrupt detected!");

  // Get more information from the MCP from the INT
  uint8_t pin = mcp.getLastInterruptPin();
  uint8_t val = mcp.getLastInterruptPinValue();

  if (operation_mode == "MANUAL") {

    if ((pin == temp_setButt) && (val == LOW)) {
      Serial.println("Please setup temperature");

      switch (butt_memo) {
        case 0: {
            butt_memo = 1;
            //mcp.digitalWrite(manu_modeLed, HIGH);
          } break;
        case 1: {
            butt_memo = 0;
            //mcp.digitalWrite(manu_modeLed, LOW);
          } break;
      }
    }
    Serial.println(butt_memo);
    if ((pin == temp_incButt) && (val == LOW)) {
      if (butt_memo == 1) {
        m_temp = m_temp + 1;
        //Serial.println("Temperature increase by 1.");
      }
    }
    if ((pin == temp_decButt) && (val == LOW)) {
      if (butt_memo == 1) {
        m_temp = m_temp - 1;
        //Serial.println("Temperature decrease by 1.");
      }
    }
    Serial.print("Current temperature is  ");
    Serial.println(m_temp);

    mcp.readGPIOAB();
  }
}

void Display2_set_temp(float temp) {
  /* This program is that displays integer and negetive integer with decimal point using TM1637 4 digit 7 segment.
    Here have referenced tm1637Display.h and TM1637test.ino files (https://github.com/avishorp/TM1637/blob/master/examples/TM1637Test/TM1637Test.ino) */

  uint8_t data[3];   // define variable for segment digit (This variable contains the value of two digits of temperature and the first digit below decimal point.)

  int v_temp = abs(temp) * 10;
  int temp1 = v_temp / 100;          // 10 digit value of reading value with DS18B20 sensor
  int temp2 = (v_temp % 100) / 10;   // 1 digit value of reading value with DS18B20 sensor
  int temp3 = (v_temp % 100) % 10;   // value for first value below the decimal point

  if (temp < 0) data[0] = 0b01000000; else data[0] = 0b00000000; // if temprature value is negative, displays data[0] as "-" sign.

  if (temp1 != 0) data[1] = display2.encodeDigit(temp1); else data[1] = 0b00000000; // if 1 digit value of temparature value is 0, don't display it.

  switch (temp2) {                          // this part is for displaying the decimal point in 1 digit dot.
    case 0: data[2] = 0b10111111; break;    // MSB bit of data[] is enable bit of dot
    case 1: data[2] = 0b10000110; break;
    case 2: data[2] = 0b11011011; break;
    case 3: data[2] = 0b11001111; break;
    case 4: data[2] = 0b11100110; break;
    case 5: data[2] = 0b11101101; break;
    case 6: data[2] = 0b11111101; break;
    case 7: data[2] = 0b10000111; break;
    case 8: data[2] = 0b11111111; break;
    case 9: data[2] = 0b11101111; break;
  }

  data[3] = display2.encodeDigit(temp3);
  display2.setSegments(data);

  if ((butt_memo == 1) && (operation_mode == "MANUAL")) { // If set_button is pressed, blink display2.
    display2.setBrightness(7, true);
    display2.setSegments(data);
    delay(TEST_DELAY);
    display2.setBrightness(7, false);
    display2.setSegments(data);
    delay(150);
  }
  else {                             // While set_button isn't pressed, cancel blink.
    display2.setBrightness(7, true);
    display2.setSegments(data);
  }
}

void Display1_curr_temp(float temp) {
  /* This program is that displays integer and negetive integer with decimal point using TM1637 4 digit 7 segment.
     Here have referenced tm1637Display.h and TM1637test.ino files (https://github.com/avishorp/TM1637/blob/master/examples/TM1637Test/TM1637Test.ino) */

  uint8_t data[3];   // define variable for segment digit (This variable contains the value of two digits of temperature and the first digit below decimal point.)

  int v_temp = abs(temp) * 10;
  int temp1 = v_temp / 100;          // 10 digit value of reading value with DS18B20 sensor
  int temp2 = (v_temp % 100) / 10;   // 1 digit value of reading value with DS18B20 sensor
  int temp3 = (v_temp % 100) % 10;   // value for first value below the decimal point

  if (temp < 0) data[0] = 0b01000000; else data[0] = 0b00000000; // if temprature value is negative, displays data[0] as "-" sign.

  if (temp1 != 0) data[1] = display1.encodeDigit(temp1); else data[1] = 0b00000000; // if 1 digit value of temparature value is 0, don't display it.

  switch (temp2) {                          // this part is for displaying the decimal point in 1 digit dot.
    case 0: data[2] = 0b10111111; break;    // MSB bit of data[] is enable bit of dot
    case 1: data[2] = 0b10000110; break;
    case 2: data[2] = 0b11011011; break;
    case 3: data[2] = 0b11001111; break;
    case 4: data[2] = 0b11100110; break;
    case 5: data[2] = 0b11101101; break;
    case 6: data[2] = 0b11111101; break;
    case 7: data[2] = 0b10000111; break;
    case 8: data[2] = 0b11111111; break;
    case 9: data[2] = 0b11101111; break;
  }

  data[3] = display1.encodeDigit(temp3);

  //display1.setBrightness(7, true);
  display1.setSegments(data);
}

/*void Blink_display() {

  display2.setBrightness(7, true);
  display2.setSegments(data);
  delay(TEST_DELAY);
  display2.setBrightness(7, false);
  display2.setSegments(data);
  delay(TEST_DELAY);

}*/

void Read_BME280() {

  bme280_temp = bme.readTemperature();
  bme280_humidity = bme.readHumidity();

  Display1_curr_temp(bme280_temp);
  //delay(1000);
  //Display1_curr_temp(bme280_humidity);

  Serial.print("Temperature = ");
  Serial.print(bme280_temp);
  Serial.println(" *C");

  Serial.print("Humidity = ");
  Serial.print(bme280_humidity);
  Serial.println(" %");

  /*Serial.print("Pressure = ");
  Serial.print(bme.readPressure() / 100.0F);
  Serial.println(" hPa");

  Serial.print("Approx. Altitude = ");
  Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
  Serial.println(" m");*/

  Serial.println();
  //delay(1000);
}

void Read_GP2Y1010() {

  float voltsMeasured;

  mcp.digitalWrite(pulseLedPin, LOW); // power on the LED
  delayMicroseconds(280);

  voltsMeasured = analogRead(dustPin); // read the dust value

  delayMicroseconds(40);
  mcp.digitalWrite(pulseLedPin, HIGH); // turn the LED off
  delayMicroseconds(9680);

  adc_vol = voltsMeasured * (3.3 / 1024);
  dustDensity = 0.17 * adc_vol - 0.1;

  Serial.print("Raw Signal Value = ");
  Serial.println(voltsMeasured);
  Serial.print("Voltage = ");
  Serial.println(adc_vol);
  Serial.print("Dust Density = ");
  Serial.print(dustDensity);
  Serial.println("mg/m3");
  Serial.println("");

}

void Read_MQ135() {

  rzero = gasSensor.getRZero(); //this to get the rzero value, uncomment this to get ppm value
  co2Density = gasSensor.getPPM(); // this to get ppm value, uncomment this to get rzero value
  Serial.print("rzero: ");
  Serial.println(rzero); // this to display the rzero value continuously, uncomment this to get ppm value
  Serial.print("CO2_density: ");
  Serial.print(co2Density); // this to display the ppm value continuously, uncomment this to get rzero value
  Serial.println("ppm");
  Serial.println();
}

void EEPROM_Write(void) {

  /* byte val_state;
   if (heatfan_power_state == "ON") {
     val_state = 1;
   }
   else if (heatfan_power_state == "OFF") {
     val_state = 0;
   }
  EEPROM.write(0, val_state);*/

  EEPROM.write(0, temp_set_value);
  EEPROM.commit();
}

void EEPROM_Read(void) {

  /*byte val_state;

  val_state = EEPROM.read(0);
  if (val_state == 1) {
    heatfan_power_state = "ON";
    digitalWrite(heatfan_powerLed, HIGH);
    digitalWrite(heatfan_powerOn, HIGH);
  }
  else if (val_state == 0) {
    heatfan_power_state = "OFF";
    digitalWrite(heatfan_powerLed, LOW);
    digitalWrite(heatfan_powerOn, LOW);
  }*/

  temp_set_value = EEPROM.read(0);

}

void loop() {
  if (!client.connected()) {
    reconnect();
    EEPROM_Read();
  }
  client.loop();

  EEPROM_Read();

  Read_BME280();
  Read_GP2Y1010();
  Read_MQ135();

  float temp_offset;

  dtostrf(bme280_temp, 4, 2, buf_bme280_temp);
  dtostrf(bme280_humidity, 4, 2, buf_bme280_humidity);
  dtostrf(dustDensity, 4, 2, buf_dustDensity);
  dtostrf(co2Density, 7, 2, buf_co2Density);
  dtostrf(temp_set_value, 4, 2, buf_set_temp_value);

  if (mcp.digitalRead(operation_modeButt) == HIGH) operation_mode = "AUTO";
  else operation_mode = "MANUAL";

  Serial.println(operation_mode);

  if (operation_mode == "AUTO") {    // in an auto operation mode

    Serial.println("Operation mode is AUTO.");
    mcp.digitalWrite(auto_modeLed, HIGH);
    mcp.digitalWrite(manu_modeLed, LOW);

    temp_offset = fabs(bme280_temp - temp_set_value);

    if (temp_offset > perOffset) {

      temp_alarm = "ON";
      mcp.digitalWrite(alarmLed, HIGH);

      if (bme280_temp > temp_set_value) {      // Then fan have to turn on

        mcp.digitalWrite(fan_powerLed, HIGH);
        mcp.digitalWrite(fan_powerOn, HIGH);
        fan_power_state = "ON";
        mcp.digitalWrite(heat_powerLed, LOW);
        mcp.digitalWrite(heat_powerOn, LOW);
        heat_power_state = "OFF";

      }
      else {                                  // Then heat have to turn on
        mcp.digitalWrite(heat_powerLed, HIGH);
        mcp.digitalWrite(heat_powerOn, HIGH);
        heat_power_state = "ON";
        mcp.digitalWrite(fan_powerLed, LOW);
        mcp.digitalWrite(fan_powerOn, LOW);
        fan_power_state = "OFF";
      }

    }
    if ((temp_offset > 0.5) && (temp_offset < perOffset)) {

      temp_alarm = "OFF";
      mcp.digitalWrite(alarmLed, LOW);
      Serial.println(temp_alarm);

      if (bme280_temp > temp_set_value) {
        mcp.digitalWrite(heat_powerLed, LOW);
        mcp.digitalWrite(heat_powerOn, LOW);
        heat_power_state = "OFF";
      }
      else {
        mcp.digitalWrite(fan_powerLed, LOW);
        mcp.digitalWrite(fan_powerOn, LOW);
        fan_power_state = "OFF";
      }
    }
    if (temp_offset < 0.5) {

      mcp.digitalWrite(alarmLed, LOW);
      mcp.digitalWrite(heat_powerLed, LOW);
      mcp.digitalWrite(heat_powerOn, LOW);
      mcp.digitalWrite(fan_powerLed, LOW);
      mcp.digitalWrite(fan_powerOn, LOW);

      heat_power_state = "OFF";
      fan_power_state = "OFF";
      temp_alarm = "OFF";

    }

    Serial.print("temp_set_value: ");
    Serial.println(temp_set_value);

    Display2_set_temp(temp_set_value);
  }

  if (operation_mode == "MANUAL") {  // in a manual operation mode

    Serial.println("Operation mode is MANUAL.");
    mcp.digitalWrite(manu_modeLed, HIGH);
    mcp.digitalWrite(auto_modeLed, LOW);

    temp_offset = fabs(bme280_temp - m_temp);

    if (mcp.digitalRead(heat_onButt) == HIGH) {

      Serial.println("Just heat onButt is pressed.");
      mcp.digitalWrite(manu_heatLed, HIGH);
      if ((temp_offset > 0.5) && (m_temp > bme280_temp)) {
        mcp.digitalWrite(heat_powerOn, HIGH);
        mcp.digitalWrite(heat_powerLed, HIGH);
        heat_power_state = "ON";
      }
      else {
        mcp.digitalWrite(heat_powerOn, LOW);
        mcp.digitalWrite(heat_powerLed, LOW);
        heat_power_state = "OFF";
      }
    }
    else {
      mcp.digitalWrite(heat_powerOn, LOW);
      mcp.digitalWrite(manu_heatLed, LOW);
      mcp.digitalWrite(heat_powerLed, LOW);
      heat_power_state = "OFF";
    }

    if (mcp.digitalRead(fan_onButt) == HIGH) {

      Serial.println("Just fan onButt is pressed.");
      mcp.digitalWrite(manu_fanLed, HIGH);
      if ((temp_offset > 0.5) && (m_temp < bme280_temp)) {
        mcp.digitalWrite(fan_powerOn, HIGH);
        mcp.digitalWrite(fan_powerLed, HIGH);
        fan_power_state = "ON";
      }
      else {
        mcp.digitalWrite(fan_powerOn, HIGH);
        mcp.digitalWrite(fan_powerLed, HIGH);
        fan_power_state = "OFF";
      }
    }
    else {
      mcp.digitalWrite(fan_powerOn, LOW);
      mcp.digitalWrite(manu_fanLed, LOW);
      mcp.digitalWrite(fan_powerLed, LOW);
      fan_power_state = "OFF";
    }

    if (temp_offset > perOffset) {

      temp_alarm = "ON";
      mcp.digitalWrite(alarmLed, HIGH);

    }

    if ((temp_offset > 0.5) && (temp_offset < perOffset)) {

      temp_alarm = "OFF";
      mcp.digitalWrite(alarmLed, LOW);

    }

    if (temp_offset < 0.5) {

      mcp.digitalWrite(alarmLed, LOW);
      mcp.digitalWrite(heat_powerLed, LOW);
      mcp.digitalWrite(heat_powerOn, LOW);
      mcp.digitalWrite(fan_powerLed, LOW);
      mcp.digitalWrite(fan_powerOn, LOW);

      heat_power_state = "OFF";
      fan_power_state = "OFF";
      temp_alarm = "OFF";

    }

    Display2_set_temp(m_temp);
  }

  //publish pubscribe
  set_pub_topic(operation_mode_state_suffix);
  client.publish(buf_pub_topic, operation_mode);

  set_pub_topic(heat_power_state_suffix);
  client.publish(buf_pub_topic, heat_power_state);

  set_pub_topic(fan_power_state_suffix);
  client.publish(buf_pub_topic, fan_power_state);

  set_pub_topic(curr_temp_suffix);
  client.publish(buf_pub_topic, buf_bme280_temp);

  set_pub_topic(curr_humidity_suffix);
  client.publish(buf_pub_topic, buf_bme280_humidity);

  set_pub_topic(temp_alarm_suffix);
  client.publish(buf_pub_topic, temp_alarm);

  set_pub_topic(set_temp_suffix);
  client.publish(buf_pub_topic, buf_set_temp_value);

  set_pub_topic(curr_dustDensity_suffix);
  client.publish(buf_pub_topic, buf_dustDensity);

  set_pub_topic(curr_CO2_density_suffix);
  client.publish(buf_pub_topic, buf_co2Density);

}

void set_pub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_pub_topic[i] = device_name[i];
    else
      buf_pub_topic[i] = suffix[i - len1];
  }
  buf_pub_topic[len1 + len2] = '\0';
}

void set_sub_topic(const char* suffix) {
  int len1 = strlen(device_name);
  int len2 = strlen(suffix);
  for (int i = 0; i < len1 + len2; i++) {
    if (i < len1)
      buf_sub_topic[i] = device_name[i];
    else
      buf_sub_topic[i] = suffix[i - len1];
  }
  buf_sub_topic[len1 + len2] = '\0';
}

