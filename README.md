This is device for controlling the power of aircon by mobile via WiFi.
Here was used NODEMCU as wifi module.
Also was used BME280 as temperature and humidity sensor, MQ135 as CO2 sensor, ADS1015 as A/D module, GY2Y1010AU0F as dust sensor
This repository consists of firmware, schematic and pcb design files.